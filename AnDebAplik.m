function varargout = AnDebAplik(varargin)
% ANDEBAPLIK MATLAB code for AnDebAplik.fig
%      ANDEBAPLIK, by itself, creates a new ANDEBAPLIK or raises the existing
%      singleton*.
%
%      H = ANDEBAPLIK returns the handle to a new ANDEBAPLIK or the handle to
%      the existing singleton*.
%
%      ANDEBAPLIK('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ANDEBAPLIK.M with the given input arguments.
%
%      ANDEBAPLIK('Property','Value',...) creates a new ANDEBAPLIK or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before AnDebAplik_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to AnDebAplik_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help AnDebAplik

% Last Modified by GUIDE v2.5 09-May-2018 12:53:58

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @AnDebAplik_OpeningFcn, ...
                   'gui_OutputFcn',  @AnDebAplik_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before AnDebAplik is made visible.
function AnDebAplik_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to AnDebAplik (see VARARGIN)

% Choose default command line output for AnDebAplik
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes AnDebAplik wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = AnDebAplik_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

set(handles.do,'String', 9)
set(handles.suwakDo,'Value',9);
set(handles.od,'String', -9)
set(handles.suwakOd,'Value',-9);
set(handles.checkwykres,'Value',1);
set(handles.checkpochodna,'Value',1);
set(handles.checkcalka,'Value',1);
checked=get(handles.checkwykres,'Value');
minimum=get(handles.suwakOd,'Value');
maksimum=get(handles.suwakDo,'Value');

if (checked)
    axes(handles.wykres)
    try
    wzor = get(handles.wzorfunkcji,'String');
    ezplot(wzor,[minimum,maksimum]);
    catch e
         h = warndlg('Blad we wzorze!');
        set(handles.wzorfunkcji,'String','x^2');
    end
else
    axes(handles.wykres)
    plot(NaN);
end

checked2=get(handles.checkpochodna,'Value');
minimum=get(handles.suwakOd,'Value');
maksimum=get(handles.suwakDo,'Value');
if (checked2)
    axes(handles.wykrespochodna);
    try
    wzor = get(handles.wzorfunkcji,'String');
    pochodna=diff(sym(wzor));
    ezplot(pochodna, [minimum maksimum]);
    catch e
         h = warndlg('Blad we wzorze!');
        set(handles.wzorfunkcji,'String','x^2');
    end
else
    axes(handles.wykrespochodna);
    plot(NaN);
end

checked3=get(handles.checkcalka,'Value');
minimum=get(handles.suwakOd,'Value');
maksimum=get(handles.suwakDo,'Value');
if (checked3)
    axes(handles.wykrescalka)
    try
     
    wzor = get(handles.wzorfunkcji,'String');
    calka=int(sym(wzor));
    ezplot(calka,[minimum,maksimum]);
    catch e
         h = warndlg('Blad we wzorze!');
        set(handles.wzorfunkcji,'String','x^2');
    end
else
    axes(handles.wykrescalka)
    plot(NaN);
    
end


function wzorfunkcji_Callback(hObject, ~, handles)
% hObject    handle to wzorfunkcji (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of wzorfunkcji as text
%        str2double(get(hObject,'String')) returns contents of wzorfunkcji as a double
wzor = get(hObject,'String');

checked=get(handles.checkwykres,'Value');
minimum=get(handles.suwakOd,'Value');
maksimum=get(handles.suwakDo,'Value');
if (checked)
    axes(handles.wykres)
    try
    wzor = get(handles.wzorfunkcji,'String');
    ezplot(wzor,[minimum,maksimum]);
    catch e
        h = warndlg('Blad we wzorze!');
        set(handles.wzorfunkcji,'String','x^2');
    end
else
    axes(handles.wykres)
    plot(NaN);
end

checked2=get(handles.checkpochodna,'Value');
minimum=get(handles.suwakOd,'Value');
maksimum=get(handles.suwakDo,'Value');
if (checked2)
    axes(handles.wykrespochodna);
    try
    wzor = get(handles.wzorfunkcji,'String');
    pochodna=diff(sym(wzor));
    ezplot(pochodna, [minimum maksimum]);
    catch e
         h = warndlg('Blad we wzorze!');
        set(handles.wzorfunkcji,'String','x^2');
    end
else
    axes(handles.wykrespochodna);
    plot(NaN);
end

checked3=get(handles.checkcalka,'Value');
minimum=get(handles.suwakOd,'Value');
maksimum=get(handles.suwakDo,'Value');
if (checked3)
    axes(handles.wykrescalka)
    try
     
    wzor = get(handles.wzorfunkcji,'String');
    calka=int(sym(wzor));
    ezplot(calka,[minimum,maksimum]);
    catch e
         h = warndlg('Blad we wzorze!');
        set(handles.wzorfunkcji,'String','x^2');
    end
else
    axes(handles.wykrescalka)
    plot(NaN);
    
end







% --- Executes during object creation, after setting all properties.
function wzorfunkcji_CreateFcn(hObject, eventdata, handles)
% hObject    handle to wzorfunkcji (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function suwakOd_Callback(hObject, eventdata, handles)
% hObject    handle to suwakOd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
wartosc1 = get(handles.suwakOd,'Value');
set(handles.od,'String', wartosc1)
%minimum=get(handles.suwakOd,'Value');
%maksimum=get(handles.suwakDo,'Value');
%    if (minimum>maksimum)
%        wartosc4=  minimum+1;
%        set(handles.suwakOd,'Value', wartosc4);
%    elseif (maksimum<minimum)
%        wartosc3= maksimum-1;
%        set(handles.suwakDo,'Value', wartosc3);
%    end
zakres_od=get(hObject,'Value'); 
zakres_do=get(handles.suwakDo,'Value');
if zakres_od >= zakres_do
zakres_do = zakres_od+1;
    set(handles.suwakDo,'Value',zakres_do)
    set(handles.do,'String',num2str(ceil(zakres_do)))
end
set(handles.od,'String',num2str(ceil(zakres_od)));

checked=get(handles.checkwykres,'Value');
minimum=get(handles.suwakOd,'Value');
maksimum=get(handles.suwakDo,'Value');
if (checked)
    axes(handles.wykres)
    try
    wzor = get(handles.wzorfunkcji,'String');
    ezplot(wzor,[minimum,maksimum]);
    catch e
         h = warndlg('Blad we wzorze!');
        set(handles.wzorfunkcji,'String','x^2');
    end
else
    axes(handles.wykres)
    plot(NaN);
end

checked2=get(handles.checkpochodna,'Value');
minimum=get(handles.suwakOd,'Value');
maksimum=get(handles.suwakDo,'Value');
if (checked2)
    axes(handles.wykrespochodna);
    try
    wzor = get(handles.wzorfunkcji,'String');
    pochodna=diff(sym(wzor));
    ezplot(pochodna, [minimum maksimum]);
    catch e
         h = warndlg('Blad we wzorze!');
        set(handles.wzorfunkcji,'String','x^2');
    end
else
    axes(handles.wykrespochodna);
    plot(NaN);
end

checked3=get(handles.checkcalka,'Value');
minimum=get(handles.suwakOd,'Value');
maksimum=get(handles.suwakDo,'Value');
if (checked3)
    axes(handles.wykrescalka)
    try
     
    wzor = get(handles.wzorfunkcji,'String');
    calka=int(sym(wzor));
    ezplot(calka,[minimum,maksimum]);
    catch e
         h = warndlg('Blad we wzorze!');
        set(handles.wzorfunkcji,'String','x^2');
    end
else
    axes(handles.wykrescalka)
    plot(NaN);
    
end

% --- Executes during object creation, after setting all properties.
function suwakOd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to suwakOd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function suwakDo_Callback(hObject, eventdata, handles)
% hObject    handle to suwakDo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
wartosc2 = get(handles.suwakDo,'Value');
set(handles.do,'String', wartosc2)
zakres_do=get(hObject,'Value'); 
round(zakres_do);
zakres_od=get(handles.suwakOd,'Value');
round(zakres_od);
if zakres_do <= zakres_od
    zakres_od=zakres_do-1;
    set(handles.suwakOd,'Value',zakres_od)
    set(handles.od,'String',num2str(ceil(zakres_od)))
end
set(handles.do,'String',num2str(ceil(zakres_do)));

checked=get(handles.checkwykres,'Value');
minimum=get(handles.suwakOd,'Value');
maksimum=get(handles.suwakDo,'Value');
if (checked)
    axes(handles.wykres)
    try
    wzor = get(handles.wzorfunkcji,'String');
    ezplot(wzor,[minimum,maksimum]);
    catch e
         h = warndlg('Blad we wzorze!');
        set(handles.wzorfunkcji,'String','x^2');
    end
else
    axes(handles.wykres)
    plot(NaN);
end

checked2=get(handles.checkpochodna,'Value');
minimum=get(handles.suwakOd,'Value');
maksimum=get(handles.suwakDo,'Value');
if (checked2)
    axes(handles.wykrespochodna);
    try
    wzor = get(handles.wzorfunkcji,'String');
    pochodna=diff(sym(wzor));
    ezplot(pochodna, [minimum maksimum]);
    catch e
         h = warndlg('Blad we wzorze!');
        set(handles.wzorfunkcji,'String','x^2');
    end
else
    axes(handles.wykrespochodna);
    plot(NaN);
end

checked3=get(handles.checkcalka,'Value');
minimum=get(handles.suwakOd,'Value');
maksimum=get(handles.suwakDo,'Value');
if (checked3)
    axes(handles.wykrescalka)
    try
     
    wzor = get(handles.wzorfunkcji,'String');
    calka=int(sym(wzor));
    ezplot(calka,[minimum,maksimum]);
    catch e
         h = warndlg('Blad we wzorze!');
        set(handles.wzorfunkcji,'String','x^2');
    end
else
    axes(handles.wykrescalka)
    plot(NaN);
    
end


% --- Executes during object creation, after setting all properties.
function suwakDo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to suwakDo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in checkwykres.
function checkwykres_Callback(hObject, eventdata, handles)
% hObject    handle to checkwykres (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkwykres
checked=get(handles.checkwykres,'Value');
minimum=get(handles.suwakOd,'Value');
maksimum=get(handles.suwakDo,'Value');
if (checked)
    axes(handles.wykres)
    try
    wzor = get(handles.wzorfunkcji,'String');
    ezplot(wzor,[minimum,maksimum]);
    catch e
         h = warndlg('Blad we wzorze!');
        set(handles.wzorfunkcji,'String','x^2');
    end
else
    axes(handles.wykres)
    plot(NaN);
end


% --- Executes on button press in checkpochodna.
function checkpochodna_Callback(hObject, eventdata, handles)
% hObject    handle to checkpochodna (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkpochodna
checked2=get(handles.checkpochodna,'Value');
minimum=get(handles.suwakOd,'Value');
maksimum=get(handles.suwakDo,'Value');
if (checked2)
    axes(handles.wykrespochodna);
    try
    wzor = get(handles.wzorfunkcji,'String');
    pochodna=diff(sym(wzor));
    ezplot(pochodna, [minimum maksimum]);
    catch e
         h = warndlg('Blad we wzorze!');
        set(handles.wzorfunkcji,'String','x^2');
    end
else
    axes(handles.wykrespochodna);
    plot(NaN);
end

% --- Executes on button press in checkcalka.
function checkcalka_Callback(hObject, eventdata, handles)
% hObject    handle to checkcalka (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkcalka
checked3=get(handles.checkcalka,'Value');
minimum=get(handles.suwakOd,'Value');
maksimum=get(handles.suwakDo,'Value');
if (checked3)
    axes(handles.wykrescalka)
    try
     
    wzor = get(handles.wzorfunkcji,'String');
    calka=int(sym(wzor));
    ezplot(calka,[minimum,maksimum]);
    catch e
         h = warndlg('Blad we wzorze!');
        set(handles.wzorfunkcji,'String','x^2');
    end
else
    axes(handles.wykrescalka)
    plot(NaN);
    
end


% --- Executes during object creation, after setting all properties.
function wykrespochodna_CreateFcn(hObject, eventdata, handles)
% hObject    handle to wykrespochodna (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate wykrespochodna
